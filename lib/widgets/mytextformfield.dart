import 'package:flutter/material.dart';

class MyTextFormField extends StatelessWidget {
  // final TextEditingController controller;
  final String title;
  MyTextFormField({required this.title});
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 52,
      child: TextField(
        decoration: InputDecoration(
          labelText: title,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
            borderSide: BorderSide(
              color: Color(0xFFAAABB0),
            ),
          ),
          // prefixIcon: Icon(Icons.account_box_sharp),
        ),
      ),
    );
  }
}
