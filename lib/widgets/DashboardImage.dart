import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class DashboardImage extends StatefulWidget {
  final ImageProvider image;
  final String title;
  DashboardImage({required this.title, required this.image});

  @override
  _DashboardImageState createState() => _DashboardImageState();
}

class _DashboardImageState extends State<DashboardImage> {
  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.cyanAccent,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      elevation: 4,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image(
            image: widget.image,
            height: 90.0,
            width: 90.0,
          ),
          SizedBox(
            height: 8,
          ),
          Text(
            widget.title,
            style: TextStyle(
              fontFamily: 'Montserrat Regular',
              fontSize: 16,
              color: Color.fromRGBO(63, 63, 63, 1),
            ),
          ),
        ],
      ),
    );
  }
}
