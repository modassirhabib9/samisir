import 'package:flutter/material.dart';
import 'package:wild_life_pro/screens/dashboard.dart';
import 'package:wild_life_pro/widgets/mytextformfield.dart';

class SurveyPage extends StatefulWidget {
  const SurveyPage({Key? key}) : super(key: key);

  @override
  _SurveyPageState createState() => _SurveyPageState();
}

class _SurveyPageState extends State<SurveyPage> {
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool obscureText = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: new Text("Survey"),
        backgroundColor: Colors.lightBlue,
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: SingleChildScrollView(
          child: SafeArea(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                MyTextFormField(title: "Division"),
                SizedBox(height: 10),
                MyTextFormField(title: "District"),
                SizedBox(height: 10),
                MyTextFormField(title: "Range"),
                SizedBox(height: 10),
                MyTextFormField(title: "Union Council"),
                SizedBox(height: 10),
                MyTextFormField(title: "Village/Particular Area"),
                SizedBox(height: 10),
                MyTextFormField(title: "Name of Species"),
                SizedBox(height: 10),
                MyTextFormField(title: "Sex of Specie"),
                SizedBox(height: 10),
                MyTextFormField(title: "Age of Specie"),
                SizedBox(height: 10),
                MyTextFormField(title: "Season of Survey"),
                SizedBox(height: 10),
                MyTextFormField(title: "Habitat of the Area "),
                SizedBox(height: 10),
                MyTextFormField(title: "Survey Result"),
                SizedBox(height: 10),
                MyTextFormField(title: "Evaluation of the Area"),
                SizedBox(height: 10),
                MyTextFormField(title: "From Date"),
                SizedBox(height: 10),
                MyTextFormField(title: "To Date"),
                // SizedBox(height: 10),
                // Container(
                //   height: 52,
                //   child: TextField(
                //     obscureText: obscureText,
                //     decoration: InputDecoration(
                //       suffixIcon: GestureDetector(
                //         onTap: () {
                //           setState(() {
                //             obscureText = !obscureText;
                //           });
                //         },
                //         child: Icon(
                //           obscureText == true
                //               ? Icons.visibility_outlined
                //               : Icons.visibility_off_outlined,
                //         ),
                //       ),
                //       labelText: 'Password',
                //       border: OutlineInputBorder(
                //           borderRadius: BorderRadius.circular(10),
                //           borderSide: BorderSide(color: Color(0xFFAAABB0))),
                //       // prefixIcon: Icon(Icons.lock),
                //     ),
                //   ),
                // ),
                SizedBox(height: 10),
                Container(
                  height: 50,
                  width: double.infinity,
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    // color: Theme.of(context).primaryColor,
                    color: Color(0xFF00A0DC),
                    child: Text(
                      "Submit",
                      style: TextStyle(color: Colors.white, fontSize: 16),
                    ),
                    onPressed: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => HomeScreen()));
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
