import 'package:dropdown_formfield/dropdown_formfield.dart';
import 'package:flutter/material.dart';
import 'package:wild_life_pro/widgets/circulardropdownmenu.dart';
import 'package:wild_life_pro/widgets/circulardropdownmenu1.dart';
import 'package:wild_life_pro/widgets/circulardropdownmenu2.dart';

class LicensesPage extends StatefulWidget {
  const LicensesPage({Key? key}) : super(key: key);

  @override
  _LicensesPageState createState() => _LicensesPageState();
}

class _LicensesPageState extends State<LicensesPage> {
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  late String _myActivity;
  late String _myActivityResult;
  late String _khanActivity;
  late String _khanResult;
  final formKey = new GlobalKey<FormState>();
  String _coachingLevel = 'Types of License';
  String _coachingLevel1 = 'Types of Shooting';
  String _coachingLevel2 = 'Types of Professions';
  String _coachingLevel3 = 'Types of Tapping';
  String _coachingLevel4 = 'Types of Dealing';
  String _coachingLevel5 = 'Types of Export';
  String _coachingLevel6 = 'Types of Import';

  @override
  void initState() {
    super.initState();
    _myActivity = '';
    _myActivityResult = '';
  }

  _saveForm() {
    var form = formKey.currentState;
    if (form!.validate()) {
      // form.save();
      setState(() {
        // _myActivityResult = _myActivity;
      });
    }
  }

  @override
  void initState2() {
    super.initState();
    _khanActivity = '';
    _khanResult = '';
  }

  _saveForm2() {
    var form = formKey.currentState;
    if (form!.validate()) {
      form.save();
      setState(() {
        _myActivityResult = _myActivity;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: new Text("Licenses"),
        backgroundColor: Colors.lightBlue,
      ),
      body: Padding(
          padding: const EdgeInsets.all(20.0),
          child: SingleChildScrollView(
              child: SafeArea(
            child: Form(
              key: formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        child: DropDownFormField(
                          titleText: 'Types of License ',
                          hintText: 'Please choose one',
                          value: _myActivity,
                          onSaved: (value) {
                            setState(() {
                              _myActivity = value;
                            });
                          },
                          onChanged: (value) {
                            setState(() {
                              _myActivity = value;
                            });
                          },
                          dataSource: [
                            {
                              "display": "Shooting",
                              "value": "Shooting",
                            },
                            {
                              "display": "Possession",
                              "value": "Possession",
                            },
                            {
                              "display": "Trapping",
                              "value": "Trapping",
                            },
                            {
                              "display": "Dealing",
                              "value": "Dealing",
                            },
                            {
                              "display": "Import",
                              "value": "Import",
                            },
                            {
                              "display": "Export",
                              "value": "Export",
                            },
                            {
                              "display": "Shop keeping",
                              "value": "Shop keeping",
                            },
                          ],
                          textField: 'display',
                          valueField: 'value',
                        ),
                      ),
                      // Container(
                      //   padding: EdgeInsets.all(8),
                      //   child: RaisedButton(
                      //     child: Text('Save'),
                      //     onPressed: _saveForm,
                      //   ),
                      // ),
                      // Container(
                      //   padding: EdgeInsets.all(16),
                      //   child: Text(_myActivityResult),
                      // ),
                      SizedBox(height: 20),
                      CircularDropDownMenu(
                        dropDownMenuItem: [
                          DropdownMenuItem(
                            child: GestureDetector(
                              onTap: () {},
                              child: Text('Small Game'),
                            ),
                            value: 'Small Game',
                          ),
                          DropdownMenuItem(
                            child: GestureDetector(
                              onTap: () {},
                              child: Text('Big Game'),
                            ),
                            value: 'Big Game',
                          ),
                        ],
                        onChanged: (value) {
                          setState(() {
                            _coachingLevel1 = value;
                          });
                        },
                        hintText: _coachingLevel1,
                      ),
                      SizedBox(height: 20),
                      ///////////////////End////////////////////////////////////////
                      CircularDropDownMenu(
                        dropDownMenuItem: [
                          DropdownMenuItem(
                            child: GestureDetector(
                              onTap: () {},
                              child: Text('Black Partridge'),
                            ),
                            value: 'Black Partridge',
                          ),
                          DropdownMenuItem(
                            child: GestureDetector(
                              onTap: () {},
                              child: Text('Grey Partridge'),
                            ),
                            value: 'Grey Partridge',
                          ),
                          DropdownMenuItem(
                            child: GestureDetector(
                              onTap: () {},
                              child: Text('Chukor'),
                            ),
                            value: 'Chukor',
                          ),
                          DropdownMenuItem(
                            child: GestureDetector(
                              onTap: () {},
                              child: Text('Partridge'),
                            ),
                            value: 'Partridge',
                          ),
                          DropdownMenuItem(
                            child: GestureDetector(
                              onTap: () {},
                              child: Text('See Partridge'),
                            ),
                            value: 'See Partridge',
                          ),
                          DropdownMenuItem(
                            child: GestureDetector(
                              onTap: () {},
                              child: Text('Quail Partridge'),
                            ),
                            value: 'Quail Partridge',
                          ),
                          DropdownMenuItem(
                            child: GestureDetector(
                              onTap: () {},
                              child: Text('Crane Partridge'),
                            ),
                            value: 'Crane Partridge',
                          ),
                          DropdownMenuItem(
                            child: GestureDetector(
                              onTap: () {},
                              child: Text('Grey Hounds'),
                            ),
                            value: 'Grey Hounds',
                          ),
                          DropdownMenuItem(
                            child: GestureDetector(
                              onTap: () {},
                              child: Text('Pointer Dog'),
                            ),
                            value: 'Pointer Dog',
                          ),
                        ],
                        onChanged: (value) {
                          setState(() {
                            _coachingLevel2 = value;
                          });
                        },
                        hintText: _coachingLevel2,
                      ),
                      SizedBox(height: 20),
                      CircularDropDownMenu(
                        dropDownMenuItem: [
                          DropdownMenuItem(
                            child: GestureDetector(
                              onTap: () {},
                              child: Text('Quail Partridge'),
                            ),
                            value: 'Quail Partridge',
                          ),
                          DropdownMenuItem(
                            child: GestureDetector(
                              onTap: () {},
                              child: Text('Crane'),
                            ),
                            value: 'Crane',
                          ),
                        ],
                        onChanged: (value) {
                          setState(() {
                            _coachingLevel3 = value;
                          });
                        },
                        hintText: _coachingLevel3,
                      ),
                      SizedBox(height: 20),
                      CircularDropDownMenu2(
                        dropDownMenuItem: [
                          DropdownMenuItem(
                            child: GestureDetector(
                              onTap: () {},
                              child: Text('Quail Partridge'),
                            ),
                            value: 'Quail Partridge',
                          ),
                          DropdownMenuItem(
                            child: GestureDetector(
                              onTap: () {},
                              child: Text('Love Birds'),
                            ),
                            value: 'Love Birds',
                          ),
                          DropdownMenuItem(
                            child: GestureDetector(
                              onTap: () {},
                              child: Text('Parrot'),
                            ),
                            value: 'Parrot',
                          ),
                          DropdownMenuItem(
                            child: GestureDetector(
                              onTap: () {},
                              child: Text('Myna'),
                            ),
                            value: 'Myna',
                          ),
                        ],
                        onChanged: (value) {
                          setState(() {
                            _coachingLevel4 = value;
                          });
                        },
                        hintText: _coachingLevel4,
                      ),
                      SizedBox(height: 20),
                      ///////////////////End////////////////////////////////////////
                      CircularDropDownMenu1(
                        dropDownMenuItem: [
                          DropdownMenuItem(
                            child: GestureDetector(
                              onTap: () {},
                              child: Text('Black Partridge'),
                            ),
                            value: 'Black Partridge',
                          ),
                          DropdownMenuItem(
                            child: GestureDetector(
                              onTap: () {},
                              child: Text('Grey Partridge'),
                            ),
                            value: 'Grey Partridge',
                          ),
                          DropdownMenuItem(
                            child: GestureDetector(
                              onTap: () {},
                              child: Text('Quail Partridge'),
                            ),
                            value: 'Quail Partridge',
                          ),
                          DropdownMenuItem(
                            child: GestureDetector(
                              onTap: () {},
                              child: Text('Parrot'),
                            ),
                            value: 'Parrot',
                          ),
                          DropdownMenuItem(
                            child: GestureDetector(
                              onTap: () {},
                              child: Text('Myna'),
                            ),
                            value: 'Myna',
                          ),
                          DropdownMenuItem(
                            child: GestureDetector(
                              onTap: () {},
                              child: Text('Crane'),
                            ),
                            value: 'Crane',
                          ),
                          DropdownMenuItem(
                            child: GestureDetector(
                              onTap: () {},
                              child: Text('Dove'),
                            ),
                            value: 'Dove',
                          ),
                        ],
                        onChanged: (value) {
                          setState(() {
                            _coachingLevel5 = value;
                          });
                        },
                        hintText: _coachingLevel5,
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ))),
    );
  }
}
