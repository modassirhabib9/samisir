import 'package:flutter/material.dart';
import 'package:wild_life_pro/screens/dashboard.dart';
import 'package:wild_life_pro/widgets/mytextformfield.dart';

class ProtectedAreasPage extends StatefulWidget {
  const ProtectedAreasPage({Key? key}) : super(key: key);

  @override
  _ProtectedAreasPageState createState() => _ProtectedAreasPageState();
}

class _ProtectedAreasPageState extends State<ProtectedAreasPage> {
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool obscureText = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: new Text("Protected Areas"),
        backgroundColor: Colors.lightBlue,
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: SingleChildScrollView(
          child: SafeArea(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                MyTextFormField(title: "Name"),
                SizedBox(height: 10),
                MyTextFormField(title: "Area in Hectors"),
                SizedBox(height: 10),
                MyTextFormField(title: "Location (Division)"),
                SizedBox(height: 10),
                MyTextFormField(title: "Geographical Location"),
                SizedBox(height: 10),
                MyTextFormField(title: "General Faena"),
                SizedBox(height: 10),
                MyTextFormField(title: "General Flora"),
                SizedBox(height: 10),
                MyTextFormField(title: "Faena Specie"),
                SizedBox(height: 10),
                MyTextFormField(title: "Flora Specie"),
                SizedBox(height: 10),
                MyTextFormField(title: "Longitude/Latitude"),
                SizedBox(height: 10),
                MyTextFormField(title: "Habitat Type"),
                // SizedBox(height: 10),
                // Container(
                //   height: 52,
                //   child: TextField(
                //     obscureText: obscureText,
                //     decoration: InputDecoration(
                //       suffixIcon: GestureDetector(
                //         onTap: () {
                //           setState(() {
                //             obscureText = !obscureText;
                //           });
                //         },
                //         child: Icon(
                //           obscureText == true
                //               ? Icons.visibility_outlined
                //               : Icons.visibility_off_outlined,
                //         ),
                //       ),
                //       labelText: 'Password',
                //       border: OutlineInputBorder(
                //           borderRadius: BorderRadius.circular(10),
                //           borderSide: BorderSide(color: Color(0xFFAAABB0))),
                //       // prefixIcon: Icon(Icons.lock),
                //     ),
                //   ),
                // ),
                SizedBox(height: 10),
                Container(
                  height: 50,
                  width: double.infinity,
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    // color: Theme.of(context).primaryColor,
                    color: Color(0xFF00A0DC),
                    child: Text(
                      "Submit",
                      style: TextStyle(color: Colors.white, fontSize: 16),
                    ),
                    onPressed: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => HomeScreen()));
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
