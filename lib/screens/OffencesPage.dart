import 'package:flutter/material.dart';
import 'package:wild_life_pro/screens/dashboard.dart';
import 'package:wild_life_pro/widgets/mytextformfield.dart';

class OffencesPage extends StatefulWidget {
  const OffencesPage({Key? key}) : super(key: key);

  @override
  _OffencesPageState createState() => _OffencesPageState();
}

class _OffencesPageState extends State<OffencesPage> {
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool obscureText = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: new Text("Offences"),
        backgroundColor: Colors.lightBlue,
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: SingleChildScrollView(
          child: SafeArea(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                MyTextFormField(title: "Offence ID"),
                SizedBox(height: 10),
                MyTextFormField(title: "Division"),
                SizedBox(height: 10),
                MyTextFormField(title: "District"),
                SizedBox(height: 10),
                MyTextFormField(title: "Name"),
                SizedBox(height: 10),
                MyTextFormField(title: "Address"),
                SizedBox(height: 10),
                MyTextFormField(title: "Offence Type"),
                SizedBox(height: 10),
                MyTextFormField(title: "Offence Nature"),
                SizedBox(height: 10),
                MyTextFormField(title: "Date"),
                SizedBox(height: 10),
                MyTextFormField(title: "Name of Court"),
                SizedBox(height: 10),
                MyTextFormField(title: "Amount of Court"),
                SizedBox(height: 10),
                MyTextFormField(title: "Remarks"),
                SizedBox(height: 10),
                Container(
                  height: 50,
                  width: double.infinity,
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    // color: Theme.of(context).primaryColor,
                    color: Color(0xFF00A0DC),
                    child: Text(
                      "Submit",
                      style: TextStyle(color: Colors.white, fontSize: 16),
                    ),
                    onPressed: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => HomeScreen()));
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
