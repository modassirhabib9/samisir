import 'package:flutter/material.dart';
import 'package:wild_life_pro/screens/dashboard.dart';
import 'package:wild_life_pro/widgets/mytextformfield.dart';

class DepredationRoadKills extends StatefulWidget {
  const DepredationRoadKills({Key? key}) : super(key: key);

  @override
  _DepredationRoadKillsState createState() => _DepredationRoadKillsState();
}

class _DepredationRoadKillsState extends State<DepredationRoadKills> {
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool obscureText = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: new Text("Survey"),
        backgroundColor: Colors.lightBlue,
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: SingleChildScrollView(
          child: SafeArea(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                MyTextFormField(title: "Date of Kill Found "),
                SizedBox(height: 10),
                MyTextFormField(title: "Type of Road"),
                SizedBox(height: 10),
                MyTextFormField(title: "Type of Species"),
                SizedBox(height: 10),
                MyTextFormField(title: "Location"),
                SizedBox(height: 10),
                MyTextFormField(title: "Division"),
                SizedBox(height: 10),
                Container(
                  height: 50,
                  width: double.infinity,
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    // color: Theme.of(context).primaryColor,
                    color: Color(0xFF00A0DC),
                    child: Text(
                      "Submit",
                      style: TextStyle(color: Colors.white, fontSize: 16),
                    ),
                    onPressed: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => HomeScreen()));
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
