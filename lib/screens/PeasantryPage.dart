import 'package:flutter/material.dart';
import 'package:wild_life_pro/screens/dashboard.dart';
import 'package:wild_life_pro/widgets/mytextformfield.dart';

class PeasantryPage extends StatefulWidget {
  const PeasantryPage({Key? key}) : super(key: key);

  @override
  _PeasantryPageState createState() => _PeasantryPageState();
}

class _PeasantryPageState extends State<PeasantryPage> {
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool obscureText = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: new Text("Peasantry"),
        backgroundColor: Colors.lightBlue,
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: SingleChildScrollView(
          child: SafeArea(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                MyTextFormField(title: "Code"),
                // SizedBox(height: 10),
                // Container(
                //   height: 52,
                //   child: TextField(
                //     obscureText: obscureText,
                //     decoration: InputDecoration(
                //       suffixIcon: GestureDetector(
                //         onTap: () {
                //           setState(() {
                //             obscureText = !obscureText;
                //           });
                //         },
                //         child: Icon(
                //           obscureText == true
                //               ? Icons.visibility_outlined
                //               : Icons.visibility_off_outlined,
                //         ),
                //       ),
                //       labelText: 'Password',
                //       border: OutlineInputBorder(
                //           borderRadius: BorderRadius.circular(10),
                //           borderSide: BorderSide(color: Color(0xFFAAABB0))),
                //       // prefixIcon: Icon(Icons.lock),
                //     ),
                //   ),
                // ),
                SizedBox(height: 10),
                MyTextFormField(title: "Name"),
                SizedBox(height: 10),
                MyTextFormField(title: "Date"),
                SizedBox(height: 10),
                MyTextFormField(title: "Type"),
                SizedBox(height: 10),
                MyTextFormField(title: "Division"),
                SizedBox(height: 10),
                MyTextFormField(title: "Male"),

                SizedBox(height: 10),
                MyTextFormField(title: "Chicks"),
                SizedBox(height: 10),
                MyTextFormField(title: "Eggs"),
                SizedBox(height: 10),
                MyTextFormField(title: "Amount"),
                SizedBox(height: 10),
                Container(
                  height: 50,
                  width: double.infinity,
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    // color: Theme.of(context).primaryColor,
                    color: Color(0xFF00A0DC),
                    child: Text(
                      "Submit",
                      style: TextStyle(color: Colors.white, fontSize: 16),
                    ),
                    onPressed: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => HomeScreen()));
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
